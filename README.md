# Config

自用配置文件等

bashrc
```
# To Install or Update:
# wget -O ~/.bashrc https://sourceforge.net/projects/ultimate-bashrc/files/_bashrc/download
# wget -O ~/.bashrc_help https://sourceforge.net/projects/ultimate-bashrc/files/_bashrc_help/download
# -or-
# curl -L --output ~/.bashrc https://sourceforge.net/projects/ultimate-bashrc/files/_bashrc/download
# curl -L --output ~/.bashrc_help https://sourceforge.net/projects/ultimate-bashrc/files/_bashrc_help/download
```

开源思源字体下载到 `mkdir-p  /usr/share/fonts/opentype` 目录，并 fc-cache 清空字体缓存

`wget -P /usr/share/fonts/opentype/ https://gitcode.com/targz/config/blob/main/SourceHanSansCN-Regular.otf`

`wget -P /usr/share/fonts/opentype/ https://gitcode.com/targz/config/blob/main/SourceHanSansCN-Normal.otf`